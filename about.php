<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>About me</title>
    <meta name="description" content="portfolio AYOUB TABSIL">
    <meta name="keywords" content="PORTFOLIO BLACK AYOUB TABSIL">
    
    <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
    <link rel="stylesheet" href="css/styles-merged.css">
    <link rel="stylesheet" href="css/style.min.css">
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <!-- START: header -->
  
  <div class="probootstrap-loader"></div>

  <header role="banner" class="probootstrap-header">
    <div class="container">
        <a href="index.php" class="probootstrap-logo"> <img src="img/black.png" alt=""> <span>.</span></a>
        
        <a href="#" class="probootstrap-burger-menu visible-xs" ><i>Menu</i></a>
        <div class="mobile-menu-overlay"></div>

        <nav role="navigation" class="probootstrap-nav hidden-xs">
          <ul class="probootstrap-main-nav">
            <li><a href="index.php">Home</a></li>
            <li class="active"><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
          <div class="extra-text visible-xs"> 
            <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
            <h5>Address</h5>
            <p>198 West 21th Street, Suite 721 New York NY 10016</p>
            <h5>Connect</h5>
            <ul class="social-buttons">
              <li><a href="https://www.linkedin.com/in/ayoub-tabsil-4795b5176/?originalSubdomain=ma" target="_blank"><i class="icon-linkedin" ></i></a></li>
              <li><a href="https://www.facebook.com/profile.php?id=100011414736458" target="_blank" ><i class="icon-facebook2"></i></a></li>
              <li><a href="TABSILAYOUB@GMAIL.COM" target="_blank"><i class="icon-email" ></i></a></li>
            </ul>
          </div>
        </nav>
    </div>
  </header>
  <!-- END: header -->

  <section class="probootstrap-section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <p><img src="img/C.jpg" class="img-responsive" alt=""></p>
        </div>
      </div>
      <div class="row">
        <div class="col-md-7 col-md-push-5">
          <!-- <img src="img/60734777_434065180745848_4370762044161392640_n.jpg" class="img-responsive" alt=""> -->
          <h2>About me</h2>
          <p> Je me prénomme AYOUB, 21 ans, développeur apprenant a youcode, j'ai un baccalaureat science de l'ingenieur, et j'etais un etudiant a la faculté hassane 2, ces deus formations me permettant de devlopper mes compétences en numériques.</p>
          <!-- <h2>| EXPERIENCE PROFESSIONNELE |</h2> -->
          <p>Pour pouvoir financer mes etudes j'ai travallé dans la restauration et dans le commerce, ces deux experience m'ont appris beaucoup de compétence a savoir le travail en stress, la gestionet le travail en equipe, la bonne communication;</p>
          <p> Aujourd'hui j'ai pu acquirir et maitriser les languages HTML, CSS, JS, PHP, Commandes linux de base, utilision de GIT/ GITHUB/ GIT LAB, et introduction en algorithme comme je travail avec python, Adobe after effect, adobe XD.</p>                      
        </div>
      </div>
    </div>
  </section>
  <!-- END section -->

  <footer class="probootstrap-footer" role="contentinfo">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="probootstrap-social">
            <li><a href="https://www.linkedin.com/in/ayoub-tabsil-4795b5176/?originalSubdomain=ma" target="_blank"><i class="icon-linkedin" ></i></a></li>
            <li><a href="https://gitlab.com/tabsilayoub08?nav_source=navbar" target="_blank" ><i class="icon-github2"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-chevron-thin-up"></i></a>
  </div>
  

  <script src="js/scripts.min.js"></script>
  <script src="js/main.min.js"></script>
  <script src="js/custom.js"></script>
  </body>
</html>