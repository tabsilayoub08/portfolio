<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>index</title>
    <meta name="description" content="Tabsil ayoub Portfolio ">
    <meta name="keywords" content="PORTFOLIO BLACK AYOUB TABSIL">
    
    <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
    <link rel="stylesheet" href="css/styles-merged.css">
    <link rel="stylesheet" href="css/style.min.css">
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <!-- START: header -->
  
  <div class="probootstrap-loader"></div>

  <header role="banner" class="probootstrap-header">
    <div class="container">
      <a href="index.php" class="probootstrap-logo"> <img src="img/black.png" alt=""> <span>.</span></a>
        
        <a href="#" class="probootstrap-burger-menu visible-xs" ><i>Menu</i></a>
        <div class="mobile-menu-overlay"></div>

        <nav role="navigation" class="probootstrap-nav hidden-xs">
          <ul class="probootstrap-main-nav">
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="about.php">About</a></li>
            <li><a href="contact.php">Contact</a></li>
          </ul>
          <div class="extra-text visible-xs"> 
            <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
            <h5>Address</h5>
            <p>198 West 21th Street, Suite 721 New York NY 10016</p>
            <h5>Connect</h5>
            <ul class="social-buttons">
              <li><a href="https://www.linkedin.com/in/ayoub-tabsil-4795b5176/?originalSubdomain=ma" target="_blank"><i class="icon-linkedin" ></i></a></li>
              <li><a href="https://www.facebook.com/profile.php?id=100011414736458" target="_blank" ><i class="icon-facebook2"></i></a></li>
              <li><a href="TABSILAYOUB@GMAIL.COM" target="_blank"><i class="icon-email" ></i></a></li>
            </ul>
          </div>
        </nav>
    </div>
  </header>
  <!-- END: header -->
  
  <div class="probootstrap-section">
    <div class="container text-center">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 mb40">
          <h2></h2>
          <p></p>    
        </div>
      </div>

      <div class="row probootstrap-gutter16">
        <div class="col-md-4 probootstrap-animate" data-animate-effect="fadeIn">
          <a href="index.php" class="img-bg" style="background-image: url(img/css.jpg);">
            <div class="probootstrap-photo-details">
              <h2>A turquoise colored sea</h2>
              <p>Landscape</p>
            </div>
          </a>
        </div>
        <div class="col-md-8 probootstrap-animate" data-animate-effect="fadeIn">
          <a href="single-page.html" class="img-bg" style="background-image: url(img/2Capture.jpg);">
            <div class="probootstrap-photo-details">
              <h2>Frozen Mount</h2>
              <p>Landscape</p>
            </div>
          </a>
        </div>
      </div>

      <div class="row probootstrap-gutter16">
        <div class="col-md-5 probootstrap-animate" data-animate-effect="fadeIn">
          <a href="single-page.html" class="img-bg" style="background-image: url(img/k1.jpg);">
            <div class="probootstrap-photo-details">
              <h2>Earthly Clay</h2>
              <p>Landscape</p>
            </div>
          </a>
        </div>
        <div class="col-md-3 probootstrap-animate" data-animate-effect="fadeIn">
          <a href="single-page.html" class="img-bg" style="background-image: url(img/2Capture.jpg);">
            <div class="probootstrap-photo-details">
              <h2>Man Captured</h2>
              <p>Landscape</p>
            </div>
          </a>
        </div>
        <div class="col-md-4 probootstrap-animate" data-animate-effect="fadeIn">
          <a href="https://gitlab.com/tabsilayoub08/restaurant-"target="_blank" class="img-bg" style="background-image: url(img/aa.jpg);">
            <div class="probootstrap-photo-details">
              <h2>Mountain with pines</h2>
              <p>Landscape</p>
            </div>
          </a>
        </div>
      </div>

       <div class="row probootstrap-gutter16">
        <div class="col-md-8 probootstrap-animate" data-animate-effect="fadeIn">
          <a href="single-page.html" class="img-bg" style="background-image: url(img/7.jpg);">
            <div class="probootstrap-photo-details">
              <h2>Mountains</h2>
              <p>Landscape</p>
            </div>
          </a>
        </div>
        <div class="col-md-4 probootstrap-animate" data-animate-effect="fadeIn">
          <a href="single-page.html" class="img-bg" style="background-image: url(img/6.jpg);">
            <div class="probootstrap-photo-details">
              <h2>Beautiful Sunset</h2>
              <p>Landscape</p>
            </div>
          </a>
        </div>
      </div>
      
    </div>
  </div>

  <footer class="probootstrap-footer" role="contentinfo">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="probootstrap-social">
            <li><a href="https://www.linkedin.com/in/ayoub-tabsil-4795b5176/?originalSubdomain=ma" target="_blank"><i class="icon-linkedin" ></i></a></li>
            <li><a href="https://gitlab.com/tabsilayoub08?nav_source=navbar" target="_blank" ><i class="icon-github2"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-chevron-thin-up"></i></a>
  </div>
  

  <script src="js/scripts.min.js"></script>
  <script src="js/main.min.js"></script>
  <script src="js/custom.js"></script>

  </body>
</html>