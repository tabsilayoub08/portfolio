<?php



if (isset($_POST['submit'])){
    
    $subject = $_POST['subject'];
    $mailForm = $_POST['email'];
    $message = $_POST['message'];

    $mailTo = "tabsilayoub@gmail.com";
    $headers = "from: ".$mailFrom;
    $txt = "YOU HAVE RECEIVED AN E-mail FROM  ".$mailForm.".\n\n".$message;

    mail($mailTo, $subject, $txt, $headers);
    header("Location:post_contact.php?mailsend");
}

?>




<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contacter</title>
    <meta name="description" content="PORTFOLIO TABSIL AYOUB">
    <meta name="keywords" content="PORTFOLIO BLACK AYOUB TABSIL">
    
    <link href="https://fonts.googleapis.com/css?family=Abel" rel="stylesheet">
    <link rel="stylesheet" href="css/styles-merged.css">
    <link rel="stylesheet" href="css/style.min.css">
    <link rel="stylesheet" href="css/custom.css">

    <!--[if lt IE 9]>
      <script src="js/vendor/html5shiv.min.js"></script>
      <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <!-- START: header -->
  
  <div class="probootstrap-loader"></div>

  <header role="banner" class="probootstrap-header">
    <div class="container">
      <a href="index.php" class="probootstrap-logo"> <img src="img/black.png" alt=""> <span>.</span></a>
        
        <a href="#" class="probootstrap-burger-menu visible-xs" ><i>Menu</i></a>
        <div class="mobile-menu-overlay"></div>

        <nav role="navigation" class="probootstrap-nav hidden-xs">
          <ul class="probootstrap-main-nav">
            <li><a href="index.php">Home</a></li>
            <li><a href="about.php">About</a></li>
            <li class="active"><a href="contact.php">Contact</a></li>
          </ul>
          <div class="extra-text visible-xs"> 
            <a href="#" class="probootstrap-burger-menu"><i>Menu</i></a>
            <h5>Address</h5>
            <p>4 rue hay raja 1 , Casablanca</p>
            <h5>Connect</h5>
            <ul class="social-buttons">
              <li><a href="https://www.linkedin.com/in/ayoub-tabsil-4795b5176/?originalSubdomain=ma" target="_blank"><i class="icon-linkedin" ></i></a></li>
              <li><a href="https://www.facebook.com/profile.php?id=100011414736458" target="_blank" ><i class="icon-facebook2"></i></a></li>
              <li><a href="TABSILAYOUB@GMAIL.COM" target="_blank"><i class="icon-email" ></i></a></li>
            </ul>
          </div>
        </nav>
    </div>
  </header>
  <!-- END: header -->

  <section class="probootstrap-section">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3 mb80 text-center probootstrap-animate">
          <h2><img src="img/thanks.PNG" class="img-responsive" alt=""></h2>
          <p>Find me on social media, use the form below, or hit me up via email at tabsilayoub@gmail.com </p>
        </div>
      </div>


    </div>
  </section>  
  <!-- END section -->

  <footer class="probootstrap-footer" role="contentinfo">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <ul class="probootstrap-social">
            <li><a href="https://www.linkedin.com/in/ayoub-tabsil-4795b5176/?originalSubdomain=ma" target="_blank"><i class="icon-linkedin" ></i></a></li>
            <li><a href="https://gitlab.com/tabsilayoub08?nav_source=navbar" target="_blank" ><i class="icon-github2"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-chevron-thin-up"></i></a>
  </div>
  

  <script src="js/scripts.min.js"></script>
  <script src="js/main.min.js"></script>

  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>

  <script src="js/custom.js"></script>
  </body>
</html>
